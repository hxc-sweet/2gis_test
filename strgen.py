#!/usr/local/bin/python3.5
import sys


def increment_char(c):
    """
    Increment a character
    """
    return chr(ord(c) + 1) if c != 'z' else 'a'


def increment_str(s):
    """
    increment a string
    """
    lpart = s.rstrip('z')
    if not lpart:
        new_s = 'a' * (len(s) + 1)
    else:
        num_replacements = len(s) - len(lpart)
        new_s = lpart[:-1] + increment_char(lpart[-1])
        new_s += 'a' * num_replacements
    return new_s


def generate(str1, str2):
    if len(str1) < len(str2) or str1 < str2:
        cur_str = str1
        max_str = str2
    else:
        cur_str = str2
        max_str = str1

    while cur_str != max_str:
        print(cur_str)
        cur_str = increment_str(cur_str)
    print(max_str)

if __name__ == '__main__':
    args = sys.argv[1:3]
    generate(*args)
