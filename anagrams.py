#!/usr/local/bin/python3.5
import fileinput
from itertools import groupby


def group_by_anagrams(words):
    return (
        group for _, group in
        groupby(words, key=lambda s: sorted(s.lower()))
    )


def read_lines():
    for line in fileinput.input():
        yield line.strip()


if __name__ == '__main__':
    for group in group_by_anagrams(read_lines()):
        print(*group)
