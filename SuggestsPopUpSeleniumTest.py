import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class SuggestsPopUpSeleniumTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.get("http://2gis.ru/novosibirsk")
        self.input_element = self.driver.find_element_by_xpath(
            "//input[@class='suggest__input']"
        )

    def test_suggests_pop_up(self):
        driver = self.driver
        self.input_element.send_keys("ав")
        pop_up = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located(
                (By.XPATH,
                 "//div[@class='suggest__suggestsWrapper']")
            )
        )

        for suggest in pop_up.find_elements_by_xpath(
            "//ul[@class='suggest__suggests']/li"
        ):
            self.assertTrue(suggest.is_displayed())
            self.assertTrue(suggest.text.startswith("ав"))

    def test_search_output(self):
        driver = self.driver

        test_result = [
            "Ипподромская, улица\nг. Новосибирск",
            "Ипподромская, улица\nрабочий пос. Станционно-Ояшинский (Мошковский район)",
            "Ипподромская, улица\nрабочий пос. Коченево",
        ]

        self.input_element.send_keys("ипподромская")
        self.input_element.submit()

        results_list = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located(
                (By.CSS_SELECTOR,
                 ".mixedResults__list.mixedResults__geoResults._active")
            )
        )

        for result in results_list.find_elements_by_css_selector(
            ".mixedResults__list.mixedResults__geoResults._active article"
        ):
            self.assertIn(result.text, test_result)

    def tearDown(self):
        self.input_element.clear()

    @classmethod
    def tearDownClass(self):
        self.driver.close()
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
