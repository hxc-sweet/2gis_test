#!/usr/local/bin/python3.5
import subprocess
from collections import Counter


if __name__ == '__main__':
    call_result = subprocess.run(
        ['man', 'man'], stdout=subprocess.PIPE, universal_newlines=True
    )
    words_count = Counter(call_result.stdout.split())
    print(words_count['man'])
