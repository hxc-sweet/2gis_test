1. Для проверки задач 1 и 2 
    python utils.py
2. Для проверки задачи 3
    python countman.py
3. Для проверки задачи 4
    python SuggestsPopUpSeleniumTest.py
4. Для проверки задачи 5
    python strgen.py zzy aaab
5. Для проверки задачи 6 (Трудоёмкость равна O(n))
    python anagrams.py data_file.txt
