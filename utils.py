import unittest
from collections import Counter


def count_unique(lst):
    return Counter(lst)


def powers(max_power):
    return (lambda x: pow(x, power) for power in range(max_power + 1))


class UtilsTestCase(unittest.TestCase):

    def test_count_unique(self):
        dct = {1: 1, 2: 2, 3: 3, 4: 3, 5: 1}
        self.assertDictEqual(
            count_unique([1, 2, 2, 3, 3, 3, 4, 4, 4, 5]),
            dct
        )

    def test_powers(self):
        test_list = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048,
                     4096, 8192, 16384, 32768, 65536]

        self.assertListEqual([power(2) for power in powers(16)], test_list)


if __name__ == '__main__':
    unittest.main()
